import { ADD_PLACE,CHANGE_STATUS } from './types';

export const addPlace = (placeName,status) => {
    return {
        type: ADD_PLACE,
        name: placeName,
        status: status
    }
};

export const changeStatusPlace = (key) => {
    return {
        type: CHANGE_STATUS,
        key: key
    }
};