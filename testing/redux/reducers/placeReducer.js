import { ADD_PLACE,CHANGE_STATUS } from '../actions/types';
const initialState = {
    places: []
};

const placeReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_PLACE:
            return {
                ...state,
                places: state.places.concat({
                    key: Math.random(),
                    placeName: action.name,
                    status: action.status
                })
            };
        case CHANGE_STATUS:
            return {
                ...state,
                places: state.places.map(
                    place => {
                        if(place.key == action.key)                                                                        
                            return {
                                key: place.key,
                                placeName: place.placeName,
                                status: !place.status
                            };
                        else{
                            return place;                               
                        }
                    }
                )
            };
            
            
        default:
            return state;
    }
};

export default placeReducer;