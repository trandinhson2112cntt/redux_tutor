/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {TextInput,FlatList, StyleSheet, Button,Text, View} from 'react-native';
import { connect } from 'react-redux';
import ListItem from './src/components/ListItem';
import {addPlace, changeStatusPlace} from './redux/actions/place';
class App extends Component {

  state = {
    placeName: '',
    places: []
  }

  placeSubmitHandler = () => {
    if(this.state.placeName.trim() === '') {
      return;
    }
    this.props.add(this.state.placeName,true);
  }

  placeNmaeChangeHandler = (value) => {
    this.setState({
      placeName: value
    });
  }

  changeStatus = (key) => {
    this.props.changeStatus(key);
  }


  render() {
    return (
      <View style={ styles.container }>
      <Text>{JSON.stringify(this.props.placeses)}</Text>
        <View style = { styles.inputContainer }>
          <TextInput
            placeholder = "Seach Places"
            style = { styles.placeInput }
            value = { this.state.placeName}
            onChangeText = { this.placeNmaeChangeHandler }
          ></TextInput>
          <Button title = 'Add' 
              style = { styles.placeButton }
              onPress = { this.placeSubmitHandler }
          />
          </View>
          <View style = {styles.listContainer}>
            <FlatList style = { styles.listContainer }
              data = { this.props.placeses }
              keyExtractor = {(item, index ) => index.toString()}
              renderItem = { info => (
                <ListItem
                  placeName = {info.item.placeName}
                  status = {info.item.status}
                  onPress = {() => this.changeStatus(info.item.key)}
                />
              )}
            />
          </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    placeses: state.pla.places
  }
};

const mapDispatchToProps = dispatch => {
  return {
    add: (name,status) => {
      dispatch(addPlace(name,status))
    },
    changeStatus: (key) => {
      dispatch(changeStatusPlace(key))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%'
  },
  placeInput: {
    width: '70%'
  },
  placeButton: {
    width: '30%'
  },
  listContainer: {
    width: '100%'
  }
});